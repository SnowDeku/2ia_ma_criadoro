/*

@Jorge Criado Ros
@SnowDeku

*/


#include "../include/adt_vector.h"
#include "../src/adt_vector.c"

void print_all(MemoryNode* node){

    node->ops_->MemSet(node,0);
    node->ops_->Print(node);
}

int main () {

    Vector *test_vector;
    Vector *test_vector2;

    test_vector = VECTOR_Create();
    test_vector2 = VECTOR_Create();

    VECTOR_Init(test_vector,3);
    VECTOR_Init(test_vector2,4);
    
    int* a = (int*)malloc(sizeof(int));
    int* b = (int*)malloc(sizeof(int));
    int* c = (int*)malloc(sizeof(int));
    int* d = (int*)malloc(sizeof(int));
    int* e = (int*)malloc(sizeof(int));

    *a = 65;
    *b = 66;
    *c = 67;
    *d = 68;
    *e = 69;


    printf("\nSetData First Vector : \n");
    Insert(test_vector,a,0,sizeof(int));
    Insert(test_vector,b,1,sizeof(int));
    Insert(test_vector,c,2,sizeof(int));
    Insert(test_vector,d,3,sizeof(int));
    InsertFirst(test_vector,e,sizeof(int));
    Insert(test_vector2,e,0,sizeof(int));
    Insert(test_vector2,d,1,sizeof(int));
    Insert(test_vector2,c,2,sizeof(int));
    Insert(test_vector2,b,3,sizeof(int));
    InsertFirst(test_vector2,a,sizeof(int));
    Print(test_vector);
    Print(test_vector2);

    printf("\nConCat \n");
    Concat(test_vector2,test_vector);

    Print(test_vector);


    printf("\nClean with a Outer Func: \n");
   VECTOR_Traverse(test_vector,print_all);


    return 0 ;  
}