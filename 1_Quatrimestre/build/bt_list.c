/*

@Jorge Criado Ros
@SnowDeku

*/

#include "../include/adt_list.h"
#include "../src/adt_list.c"

void print_all(MemoryNode* node){

    node->ops_->MemSet(node,0);
    node->ops_->Print(node);
}


int main(){


    List *test_list;
    test_list = LIST_Create();
    LIST_Init(test_list);

    List *test_list2;
    test_list2 = LIST_Create();
    LIST_Init(test_list2);

    int* a = (int*)malloc(sizeof(int));
    int* b = (int*)malloc(sizeof(int));
    int* c = (int*)malloc(sizeof(int));
    int* d = (int*)malloc(sizeof(int));
    int* e = (int*)malloc(sizeof(int));

    *a = 65;
    *b = 66;
    *c = 67;
    *d = 68;
    *e = 69;

    printf("\nSetData First List : \n");
    InsertLast(test_list,a,sizeof(int));
    InsertLast(test_list,b,sizeof(int));
    InsertLast(test_list,c,sizeof(int));
    InsertLast(test_list,d,sizeof(int));
    InsertFirst(test_list,e,sizeof(int));
     Print(test_list);

    printf("\nSetData Second List : \n");
    InsertLast(test_list2,a,sizeof(int));
    InsertLast(test_list2,b,sizeof(int));
    InsertLast(test_list2,c,sizeof(int));
    InsertLast(test_list2,d,sizeof(int));
    InsertFirst(test_list2,e,sizeof(int));
    Print(test_list2);

    printf("\nExtract : \n");
    MemoryNode *extracted ;
    extracted = Extract(test_list,0);
    extracted->ops_->Print(extracted);

    printf("\nList : \n");
    Print(test_list);

    printf("\nExtract : \n");
    extracted = Extract(test_list,3);
    extracted->ops_->Print(extracted);
    
    printf("\nList : \n");
    Print(test_list);
    
    printf("\nConcat : \n");
    Concat(test_list,test_list2);
    Print(test_list2);

    printf("\nClean with a Outer Func: \n");
    List_Traverse(test_list2,print_all);
    
    return 0;
}