/*

@Jorge Criado Ros
@SnowDeku

*/

#include "../include/adt_dlist.h"
#include "../src/adt_dlist.c"



int main(){

    
    DList *test_list;
    test_list = DLIST_Create();
    DLIST_Init(test_list);

    DList *test_list2;
    test_list2 = DLIST_Create();
    DLIST_Init(test_list2);

    int* a = (int*)malloc(sizeof(int));
    int* b = (int*)malloc(sizeof(int));
    int* c = (int*)malloc(sizeof(int));
    int* d = (int*)malloc(sizeof(int));
    int* e = (int*)malloc(sizeof(int));

    *a = 65;
    *b = 66;
    *c = 67;
    *d = 68;
    *e = 69;

    printf("\nSetData First DList : \n");
    InsertLast(test_list,a,sizeof(int));
    InsertLast(test_list,b,sizeof(int));
    InsertLast(test_list,c,sizeof(int));
    InsertLast(test_list,d,sizeof(int));
    InsertLast(test_list,e,sizeof(int));

    Print(test_list);
    printf("\nSplit DList: \n");
    test_list2 = Split(test_list,3);
    Print(test_list);
    printf("\nSplit DList2: \n");
    Print(test_list2);


    printf("\n Testeo : \n");
    InsertLast(test_list,a,sizeof(int));
    Print(test_list);

    printf("\n TEsteo 2: \n");
    InsertFirst(test_list2,a,sizeof(int));
    Print(test_list2);

    
    return 0;
}