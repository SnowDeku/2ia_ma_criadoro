/*

@Jorge Criado Ros
@SnowDeku

*/


#include "../include/memory_node.h"
#include "../src/memory_node.c"



int main () {

    int* a = (int*)malloc(sizeof(int));
    int* b = (int*)malloc(sizeof(int));
    int* c = (int*)malloc(sizeof(int));


    *a = 65;
    *b = 66;
    *c = 67;

    //Bateria  de Errores Memo
    MemoryNode *node_test = MEMNODE_Create();
    MemoryNode *node_test2 = MEMNODE_Create();

    MemoryNode* node_ref; 
    MEMNODE_CreateFromRef(&node_ref);

    printf("\nSetData\n");
    node_ref->ops_->setData(node_ref,a,sizeof(int));
	node_test->ops_->setData(node_test,b,sizeof(int));
    node_test2->ops_->setData(node_test2,c,sizeof(int));

    node_ref->ops_->Print(node_ref);
    node_test->ops_->Print(node_test);
    node_test->ops_->Print(node_test2);

    printf("\nMemSet\n");
    node_test->ops_->MemSet(node_test,78);

    node_test->ops_->Print(node_test);
    node_ref->ops_->Print(node_ref);
    printf("\nMemCopy\n");
    node_test->ops_->MemCopy(node_test,node_test2->data_,node_test2->size_);

    node_test->ops_->Print(node_test);

    printf("\nMemConcat\n");

    node_test->ops_->MemConcat(node_test,b,sizeof(int));

    node_test->ops_->Print(node_test);

    printf("\nConcat 2 MemConcat\n");
     node_ref->ops_->Print(node_ref);
    node_ref->ops_->MemConcat2(node_test,node_ref);

    node_ref->ops_->Print(node_ref);

    printf("\nNode_ref test: \n");

    node_ref->ops_->Print(node_ref);




    return 0 ;  
}