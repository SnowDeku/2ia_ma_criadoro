// adt_queue.c : 
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//

#include "../include/adt_queue.h"
#include "memory_node.c"


Queue* QUEUE_Create(){

  Queue *queue;
  //Reservo el espacio en memoria para el v
  queue = (Queue *) malloc(sizeof(Queue));
  queue->head_ = 0;
  queue->tail_ = 0;
  queue->capacity_ = 0;
  queue->node_  = NULL;

  return queue;
}

s16 QUEUE_Init(Queue *queue ,u16 capacity){

  //Size of the queue
  queue->capacity_ = capacity;
  //Save memory * the capacity to create a queue of mem_nodes
  queue->node_  = malloc(sizeof(MemoryNode)*capacity);

  MemoryNode *aux ;
  aux = queue->node_;
  for(u8 i = 0; i < capacity ; i++){
    aux[i] = *MEMNODE_Create();
    
  }

  return kErrorCode_Memory;
}

u16 QUEUE_Capacity(Queue *queue){
  return queue->capacity_;
}

u16 Destroy(Queue *queue){

    for(int i = queue->tail_ ; i > 0 ; --i){

    free(&queue->node_[i]);

 }
  free(queue);
 return kErrorCode_Ok;
}

bool isEmpty(Queue *queue){
    if(queue->tail_ == 0){
      return true;
    }else{
      return false;
    }
}

bool isFull(Queue *queue){
    if(queue->tail_ == queue->capacity_){
      return true;
    }else{
      return false;
    }
}
u16 Length(Queue *queue){
  return queue->tail_;
}

Queue *Resize(Queue *queue,u16 new_capacity_){
  printf("\n Resizing queue... %d\n",new_capacity_);
  //Create a aux queue were we going to save all the data
    u16 aux_tail = queue->tail_;
   MemoryNode *node_test = malloc(sizeof(MemoryNode)*new_capacity_);


  for(u8 i = 0; i < new_capacity_ ; i++){
    node_test[i] = *MEMNODE_Create();

  }
  for(u8 i = 0; i < aux_tail ; i++){
    node_test[i] = queue->node_[i];
  }
  queue->node_ = node_test;
  queue->capacity_ = new_capacity_;
  queue->tail_ = aux_tail;

  return queue;
}


s16 InsertLast(Queue *queue,void *value,u16 bytes){

   Insert(queue, value, queue->tail_,bytes);

  return kErrorCode_Ok;

}

s16 Insert(Queue *queue, void *value, u16 position_,u16 bytes){

  MemoryNode *aux;
  aux = queue->node_;
   u8 change_tail_ = queue->tail_;
  //If just want to add a new value on the array 1 means Next to increase tail_

if(queue->tail_ <= queue->capacity_){

   //Change a especific position
       if( position_ <= queue->capacity_){
          for(int i = queue->tail_; i  >= position_ ; --i){
                    queue->node_[i+1] = queue->node_[i];
               }
                queue->node_[position_].ops_->setData(&queue->node_[position_],value,bytes);
               queue->tail_++;           
       }
     
  }else{
          printf(" \nThe queue is full Resize for insert more data\n");

  }

  return kErrorCode_Ok;
}

 s16 Print(Queue *queue){
    MemoryNode *node_test ;

    node_test = queue->node_;

     for(int i = 0; i< queue->tail_ ; i++){
        node_test->ops_->Print(&node_test[i]);
    }

    node_test = NULL;
return kErrorCode_Ok;
}

 MemoryNode **Head(Queue *queue){// Head: returns a reference to the first node
  return &(&queue->node_[0]); 
}

 MemoryNode *ExtractFirst(Queue *queue){
  return Extract(queue,queue->head_);
}

 MemoryNode *Extract(Queue *queue, u16 position_){
  
 if(position_ <= queue->capacity_){
      MemoryNode *aux = MEMNODE_Create() ;
       *aux = queue->node_[position_];
        //Extract a especific node
       for(int i = position_; i  < queue->tail_ ; ++i){
                  (queue->node_[i]) =
                   (queue->node_[i+1]);         
             }
               queue->tail_--;

    return aux;
 }
  

  MemoryNode *kError_Node = NULL;
  printf("\n Error trying to Extract a NULL NODE\n");
  return kError_Node;
}
u16 Concat(Queue* src,Queue* dest){
  MemoryNode *aux;
   Resize(dest,(dest->tail_+src->tail_)-2);
       for(int i = 0; i< src->tail_ ; i++){
            
            aux = Extract(src,i);
            InsertLast(dest,aux->data_,aux->size_);
  }
            free(aux);
            aux = NULL;
  
return kErrorCode_Ok;
}
u16 QUEUE_Traverse(Queue *queue, void(*callback) (MemoryNode *)){

  for(int i = 0; i < queue->tail_ ; i++){

     callback(&queue->node_[i]);

 }

return kErrorCode_Ok;
}
