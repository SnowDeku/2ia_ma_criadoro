// prj_list.c : Defines the entry point for the console application.
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//


#include "../include/adt_list.h"
#include "memory_node.c"

List* LIST_Create(){
	List *new_list;
	new_list =  malloc(sizeof(List));
	new_list->head_ = NULL;
	new_list->tail_ = NULL;
	new_list->total_elem_= 0;

	return new_list;
}
 s16 LIST_Init(List *list){

	MemoryNode *node_ = MEMNODE_Create();
	list->head_ = node_;
	list->tail_ = node_;
	list->total_elem_ = 0;

	return kErrorCode_Ok;
}

bool isEmpty(List *list){

	if(0 == list->total_elem_){
		return true;
	}
	return false;
}
u16 Length(List *list){
	return list->total_elem_;
}
s16 InsertFirst(List *list,void *value,u16 bytes){
	Insert(list,value,0,bytes);
	  return kErrorCode_Ok;
}
s16 InsertLast(List *list,void *value,u16 bytes){
	Insert(list,value,list->total_elem_,bytes);
	  return kErrorCode_Ok;
}
s16 Insert(List *list, void *value, u16 position_,u16 bytes){

	MemoryNode *aux_prev = MEMNODE_Create();

	MemoryNode *node_data_ = MEMNODE_Create();
	node_data_->ops_->setData(node_data_,value,bytes);
	
	if( position_ > 0){

		aux_prev = list->head_;

		u8 count  = 0;

		while(count < position_-1){
		aux_prev = aux_prev->next_;
			count++;
		}

		node_data_->next_ =  aux_prev->next_;

		aux_prev->next_ =  node_data_;
		if(position_ == list->total_elem_){
			list->tail_ = node_data_;
		}

	}else{
	
		node_data_->next_ = list->head_;
		list->head_ = node_data_;
	}
	

	list->total_elem_++;


 return kErrorCode_Ok;
}
// Head: returns a reference to the first node
MemoryNode **Head(List *list){
		return &list->head_;
}

MemoryNode *ExtractFirst(List *list){
	return Extract(list,0);
}
MemoryNode *ExtractLast(List *list){
	return Extract(list,list->total_elem_);
}
MemoryNode *Extract(List *list, u16 position_){

	MemoryNode *aux;
	MemoryNode *extract;
	aux = list->head_;
	if(position_ > 0){


	u8 count  = 0;

	while(count < position_-1){
		aux = aux->next_;
			count++;
	}


	extract = aux->next_;
	aux->next_ = aux->next_->next_;
	}else{
		extract = list->head_;
		list->head_ = list->head_->next_;

	}
	list->total_elem_--;
	return extract;
}
u16 Concat(List* src,List* dest){

	dest->tail_->next_=src->head_;
	dest->tail_ = src->tail_;
	dest->total_elem_ += src->total_elem_;
	return kErrorCode_Ok;
}

u16 List_Traverse(List *list, void(*callback) (MemoryNode *)){

	MemoryNode* aux = list->head_;
  for(int i = 0; i < list->total_elem_ ; i++){

     callback(aux);
	 aux = aux->next_;

 }

return kErrorCode_Ok;
}


s16 Print(List *list){

	MemoryNode *aux;
	aux = list->head_;
	for(int i = 0; i< list->total_elem_; i++){
		aux->ops_->Print(aux);
		aux = aux->next_;
	}

	return kErrorCode_Ok;
}
