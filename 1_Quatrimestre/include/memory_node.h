// memory_node.h
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//
#ifndef __MEMORY_NODE_H__
#define __MEMORY_NODE_H__

#include <stdio.h>
#include <stdlib.h>
#include "../extern/platform_types.h"
#include "../extern/common_def.h"

/////////////////////////////////////////////////
///  Structure we going to use in all of our TDA.
/////////////////////////////////////////////////

typedef struct memory_node_s
{
	void *data_;
	u16 size_;
	struct memory_node_ops_s *ops_;
	
	struct memory_node_s *next_; //!< For only  use on List and DList TDA
	struct memory_node_s *prev_; //!< For only  use on List and DList TDA

} MemoryNode;

struct memory_node_ops_s
{
	s16(*Init) (MemoryNode *node);
	void*(*Data)();
	u16(*Size)();
	MemoryNode* (*Next)();
	MemoryNode* (*Prev)();
	
	s16(*setData) (MemoryNode *node, void *src, u16 bytes);
	s16(*MemSet) (MemoryNode *node, u8 value);
	s16(*MemCopy) (MemoryNode *node, void *src, u16 bytes);
	s16(*MemCopy2) (MemoryNode *node, MemoryNode *dest);
	s16(*MemConcat) (MemoryNode *node, void *src, u16 bytes);
	s16(*MemConcat2) (MemoryNode *node, MemoryNode *dest);
	void(*Print)(MemoryNode *node);
};

MemoryNode* MEMNODE_Create();
s16 MEMNODE_CreateFromRef(MemoryNode **node);

#endif // __MEMORY_NODE_H__