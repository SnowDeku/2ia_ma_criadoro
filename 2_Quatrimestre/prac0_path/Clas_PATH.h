/* 
@Segundo de Programacíon
@Jorge Criado Ros
@Class Path.h

*/

#ifndef __PATH_H__
#define __PATH_H__ 1

#include "platform_types.h"
#include "math.h"

enum DIR { FORWARD,BACKWARD };

class PATH {

public:

//Constructor & Destructor
PATH();
~PATH();

//Funcs
Create();
Reset();
Add_Point();
isLast();
Next_Point();
Set_Direction();
Lenth();

private:

    Vec3 Point[];
    DIR Direction;
    u16 Total;
    s16 OFFSET;

};

#endif
