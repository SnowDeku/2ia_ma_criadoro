/* 
@Segundo de Programacíon
@Jorge Criado Ros
@Class Path.cc

*/
/*
varaibles of clas :
    Vec3 Point[];
    DIR Direction;
    u16 Total;
    s16 OFFSET;
*/

#include "clas_PATH.h"


//Constructor & Destructor
PATH::PATH(){

}
PATH::~PATH();

//Funcs
PATH::Create();
PATH::Reset();
PATH::Add_Point();
PATH::isLast();
PATH::Next_Point();
PATH::Set_Direction();
PATH::Lenth();


