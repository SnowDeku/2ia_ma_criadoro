/* 
@Segundo de Programacíon
@Jorge Criado Ros
@Class Path.h

*/
#pragma once

#include "../extern/ESAT_lib/inlude/ESAT/input.h"
#include "gamemanager.h"
#include "math.h"


class SIMLOOP {

public:

/*
*Constructor & Destructor
*/
SIMLOOP();
~SIMLOOP();

/*
*Functions
*/
void Start();

/* Si se pulsa la tecla ESC se se
*  informara al GM(gamemanager) que se cerrara el juego y 
*  tiene que hacer los cambios pertinentes antes de cerralo
*/
void InputService();

/* Used for the end of the loop and when is going to call the 
*InputService Function it restart the values
*/
void ResetInput();

/*
* Update used for update all the current values according to the InputService
*/
void Update(Uint32 time);

/*
* We will use it for draw all the current data
*/
void Draw();



protected:
 



private:

    /*
    * input_ :
    * forward -> W key_up
    * rigth -> D key_rigth
    * left -> A key_left
    * back -> S key_down
    * space -> key_space
    * enter -> key_enter
    * mouse_1 -> ESAT::Mouse 0 left_click
    * mouse_2 -> ESAT::Mouse 1 rigth_click
    * esc_ -> esc
    * control_ -> control
    * alt_ -> alt
    * shift_ ->  shift_
    */
bool forward_;
bool rigth_;
bool left_;
bool back_;
bool space_;
bool enter_;
bool mouse_1_;
bool mouse_2_;
bool exit_;
bool control_;
bool alt_;
bool shift_;



};
