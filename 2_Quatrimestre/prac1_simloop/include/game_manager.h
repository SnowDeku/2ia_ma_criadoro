/* 
@Segundo de Programacíon
@Jorge Criado Ros
@Class Path.h

*/
#pragma once

#include "../../extern/DTS/extern/platform_types.h"
#include "../../extern/include/ESAT_SDK/math.h"


class GAMEMANAGER{

public:

/*
*Variables
*/
bool quit_game_;
Uint32 time_step_;

/*
*Constructor & Destructor
*/
GAMEMANAGER();
~GAMEMANAGER();

/*
*Functions
*/

void setTime_step(Uint32 time);

/*
*This will chose what he will update
*/
void Update(Uint32 time);

void Draw();

protected:
 



private:


};
