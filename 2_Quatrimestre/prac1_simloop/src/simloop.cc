/* 
@Segundo de Programacíon
@Jorge Criado Ros
@Class simloop.cc

*/

#include "../include/simloop.h"

/*
*Constructor & Destructor
*/
SIMLOOP();
~SIMLOOP();


void Start(){

    u32 current_time = SDL_GetTicks();

    while(false = quit_game_){

        InputService();

        u32 accum_time = SDL_GetTicks() - current_time;

        while(accum_time >= time_step_){

            Update(time_step_);

            current_time +=time_step_;

            accum_time = SDL_GetTicks() - current_time;
        }

        Draw();
    }
}


void InputService(){

    //Before check all the input restart all the values
    ResetInput();


    if(ESAT::IsSpecialKeyPressed(kSpecialKey_Up) || ESAT::IsKeyPressed("w")){
        forward_ = true;
    }

    if(ESAT::IsSpecialKeyPressed(kSpecialKey_Down) || ESAT::IsKeyPressed("s")){
        back_ = true;
    }
    if(ESAT::IsSpecialKeyPressed(kSpecialKey_Right) || ESAT::IsKeyPressed("d")){
        rigth_ = true;
    }
    if(ESAT::IsSpecialKeyPressed(kSpecialKey_Left) || ESAT::IsKeyPressed("a")){
        left_ = true;
    }
    if(ESAT::IsSpecialKeyPressed(kSpecialKey_Space)){
        space_ = true;
    }
    if(ESAT::IsSpecialKeyPressed(kSpecialKey_Enter)){
        enter_ = true;
    }
    if(ESAT::IsSpecialKeyPressed(kSpecialKey_Control)){
        control_ = true;
    }
    if(ESAT::IsSpecialKeyPressed(kSpecialKey_Alt)){
        alt_ = true;
    }   
     if(ESAT::IsSpecialKeyPressed(kSpecialKey_Shift)){
        shift_ = true;
    } 
    if(ESAT::IsSpecialKeyPressed(kSpecialKey_Esc)){
        exit_ = true;
    } 
    if(ESAT::MouseButtonPressed(0)){
        mouse_1_ = true;
    }
    if(ESAT::MouseButtonPressed(1)){
        mouse_2_ = true;
    }

}

void ResetInput(){

 forward_ = false;
 rigth_ = false;
 left_ = false;
 back_ = false;
 space_ = false;
 enter_ = false;
 mouse_1_ = false;
 mouse_2_ = false;
 exit_ = false;
 control_ = false;
 alt_ = false;
 shift_ = false;

}

/*
* This Function will call all the Functions need to update 
*/
void Update(Unit32 time_step){
 
 gamemanage->Update(time_step);
 
//update d los 
}


/*
* This Function will call all the Functions need to draw 
*/
void Draw(){




}