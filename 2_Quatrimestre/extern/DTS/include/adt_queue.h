// adt_queue.h : 
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//

#ifndef __ADT_QUEUE_H__
#define __ADT_QUEUE_H__

#include "memory_node.h"

typedef struct adt_queue_s
{
	u16 head_;
	u16 tail_;
	u16 capacity_;
	MemoryNode *node_;
} Queue;

Queue* QUEUE_Create();
static s16 QUEUE_Init(Queue *queue ,u16 capacity);
static u16 QUEUE_Capacity(Queue *queue);
static bool isEmpty(Queue *queue);
static bool isFull(Queue *queue);
static u16 Length(Queue *queue);
static Queue* Resize(Queue *queue,u16 new_capacity_);
static s16 Insert(Queue *queue, void *value, u16 position_,u16 bytes);
static s16 InsertLast(Queue *queue,void *value,u16 bytes);
static MemoryNode **Head(Queue *queue);// Head: returns a reference to the first node
static MemoryNode *Extract(Queue *queue, u16 position_);
static MemoryNode *ExtractLast(Queue *queue);
static u16 Concat(Queue* src,Queue* dest);
u16 QUEUE_Traverse(Queue *queue, void(*callback) (MemoryNode *));// traverses a queue and applies a callback to each node
static s16 Print(Queue *queue);
#endif //__ADT_QUEUE_H__
