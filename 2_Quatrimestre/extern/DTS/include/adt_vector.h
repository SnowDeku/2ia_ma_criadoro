// adt_vector.h : 
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//

#ifndef __ADT_VECTOR_H__
#define __ADT_VECTOR_H__

#include "memory_node.h"

typedef struct adt_vector_s
{
	u16 head_;
	u16 tail_;
	u16 capacity_;
	MemoryNode *node_;
} Vector;

Vector* VECTOR_Create();
static s16 VECTOR_Init(Vector *vector ,u16 capacity);
static u16 VECTOR_Capacity(Vector *vector);
static bool isEmpty(Vector *vector);
static bool isFull(Vector *vector);
static u16 Length(Vector *vector);
static Vector* Resize(Vector *vector,u16 new_capacity_);
static s16 Insert(Vector *vector, void *value, u16 position_,u16 bytes);
static s16 InsertFirst(Vector *vector,void *value,u16 bytes);
static s16 InsertLast(Vector *vector,void *value,u16 bytes);
static MemoryNode **Head(Vector *vector);// Head: returns a reference to the first node
static MemoryNode *Extract(Vector *vector, u16 position_);
static MemoryNode *ExtractFirst(Vector *vector);
static MemoryNode *ExtractLast(Vector *vector);
static u16 Concat(Vector* src,Vector* dest);
u16 VECTOR_Traverse(Vector *vector, void(*callback) (MemoryNode *));// traverses a vector and applies a callback to each node
static s16 Print(Vector *vector);
#endif //__ADT_VECTOR_H__
