// adt_vector.h : 
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//

#ifndef __ADT_LIST_H__
#define __ADT_LIST_H__


#include "memory_node.h"


typedef struct adt_list_s
{
	MemoryNode *head_;
	MemoryNode *tail_;
	u16 total_elem_;
} List;

List* LIST_Create();
static s16 LIST_Init(List *list);
static bool isEmpty(List *list);
static u16 Length(List *list);
static List* Resize(List *list,u16 new_capacity_);
static s16 InsertFirst(List *list,void *value,u16 bytes);
static s16 InsertLast(List *list,void *value,u16 bytes);
static s16 Insert(List *list, void *value, u16 position_,u16 bytes);
static MemoryNode **Head(List *list);// Head: returns a reference to the first node
static MemoryNode *ExtractFirst(List *list);
static MemoryNode *ExtractLast(List *list);
static MemoryNode *Extract(List *list, u16 position_);
static u16 Concat(List* src,List* dest);
u16 List_Traverse(List *list, void(*callback) (MemoryNode *));// traverses a list and applies a callback to each node
static s16 Print(List *list);
#endif //__ADT_LIST_H__