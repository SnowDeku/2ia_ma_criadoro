// adt_stack.h : 
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//

#ifndef __ADT_STACK_H__
#define __ADT_STACK_H__

#include "memory_node.h"

typedef struct adt_stack_s
{
	u16 head_;
	u16 tail_;
	u16 capacity_;
	MemoryNode *node_;
} Stack;

Stack* STACK_Create();
static s16 STACK_Init(Stack *stack ,u16 capacity);
static u16 STACK_Capacity(Stack *stack);
static bool isEmpty(Stack *stack);
static bool isFull(Stack *stack);
static u16 Length(Stack *stack);
static Stack* Resize(Stack *stack,u16 new_capacity_);
static s16 Insert(Stack *stack, void *value, u16 position_,u16 bytes);
static s16 InsertLast(Stack *stack,void *value,u16 bytes);
static MemoryNode **Head(Stack *stack);// Head: returns a reference to the first node
static MemoryNode *Extract(Stack *stack, u16 position_);
static MemoryNode *ExtractLast(Stack *stack);
static u16 Concat(Stack* src,Stack* dest);
u16 STACK_Traverse(Stack *stack, void(*callback) (MemoryNode *));// traverses a stack and applies a callback to each node
static s16 Print(Stack *stack);
#endif //__ADT_STACK_H__
