// adt_vector.c : 
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//

#include "../include/adt_vector.h"
#include "memory_node.c"


Vector* VECTOR_Create(){

  Vector *vector;
  //Reservo el espacio en memoria para el v
  vector = (Vector *) malloc(sizeof(Vector));
  vector->head_ = 0;
  vector->tail_ = 0;
  vector->capacity_ = 0;
  vector->node_  = NULL;

  return vector;
}

s16 VECTOR_Init(Vector *vector ,u16 capacity){

  //Size of the vector
  vector->capacity_ = capacity;
  //Save memory * the capacity to create a vector of mem_nodes
  vector->node_  = malloc(sizeof(MemoryNode)*capacity);

  MemoryNode *aux ;
  aux = vector->node_;
  for(u8 i = 0; i < capacity ; i++){
    aux[i] = *MEMNODE_Create();
    
  }

  return kErrorCode_Memory;
}

u16 VECTOR_Capacity(Vector *vector){
  return vector->capacity_;
}

u16 Destroy(Vector *vector){

    for(int i = vector->tail_ ; i > 0 ; --i){

    free(&vector->node_[i]);

 }
  free(vector);
 return kErrorCode_Ok;
}

bool isEmpty(Vector *vector){
    if(vector->tail_ == 0){
      return true;
    }else{
      return false;
    }
}

bool isFull(Vector *vector){
    if(vector->tail_ == vector->capacity_){
      return true;
    }else{
      return false;
    }
}
u16 Length(Vector *vector){
  return vector->tail_;
}

Vector *Resize(Vector *vector,u16 new_capacity_){
  printf("\n Resizing vector... %d\n",new_capacity_);
  //Create a aux vector were we going to save all the data
    u16 aux_tail = vector->tail_;
   MemoryNode *node_test = malloc(sizeof(MemoryNode)*new_capacity_);


  for(u8 i = 0; i < new_capacity_ ; i++){
    node_test[i] = *MEMNODE_Create();

  }
  for(u8 i = 0; i < aux_tail ; i++){
    node_test[i] = vector->node_[i];
  }
  vector->node_ = node_test;
  vector->capacity_ = new_capacity_;
  vector->tail_ = aux_tail;

  return vector;
}

s16 InsertFirst(Vector *vector,void *value,u16 bytes){

  Insert(vector, value,vector->head_,bytes);

  return kErrorCode_Ok;
}

s16 InsertLast(Vector *vector,void *value,u16 bytes){


   Insert(vector, value, vector->tail_,bytes);

  return kErrorCode_Ok;

}

s16 Insert(Vector *vector, void *value, u16 position_,u16 bytes){

  MemoryNode *aux;
  aux = vector->node_;
   u8 change_tail_ = vector->tail_;
  //If just want to add a new value on the array 1 means Next to increase tail_

if(vector->tail_ <= vector->capacity_){

   //Change a especific position
       if( position_ <= vector->capacity_){
          for(int i = vector->tail_; i  >= position_ ; --i){
                    vector->node_[i+1] = vector->node_[i];
               }
                vector->node_[position_].ops_->setData(&vector->node_[position_],value,bytes);
               vector->tail_++;           
       }
     
  }else{
          printf(" \nThe vector is full Resize for insert more data\n");

  }

  return kErrorCode_Ok;
}

 s16 Print(Vector *vector){
    MemoryNode *node_test ;

    node_test = vector->node_;

     for(int i = 0; i< vector->tail_ ; i++){
        node_test->ops_->Print(&node_test[i]);
    }

    node_test = NULL;
return kErrorCode_Ok;
}

 MemoryNode **Head(Vector *vector){// Head: returns a reference to the first node
  return &(&vector->node_[0]); 
}

 MemoryNode *ExtractFirst(Vector *vector){
  return Extract(vector,vector->head_);
}

 MemoryNode *ExtractLast(Vector *vector){
  return Extract(vector,vector->tail_-1);
}

 MemoryNode *Extract(Vector *vector, u16 position_){
  
 if(position_ <= vector->capacity_){
      MemoryNode *aux = MEMNODE_Create() ;
       *aux = vector->node_[position_];
        //Extract a especific node
       for(int i = position_; i  < vector->tail_ ; ++i){
                  (vector->node_[i]) =
                   (vector->node_[i+1]);         
             }
               vector->tail_--;

    return aux;
 }
  

  MemoryNode *kError_Node = NULL;
  printf("\n Error trying to Extract a NULL NODE\n");
  return kError_Node;
}
u16 Concat(Vector* src,Vector* dest){
  MemoryNode *aux;
   Resize(dest,(dest->tail_+src->tail_)-2);
       for(int i = 0; i< src->tail_ ; i++){
            
            aux = Extract(src,i);
            InsertLast(dest,aux->data_,aux->size_);
  }
            free(aux);
            aux = NULL;
  
return kErrorCode_Ok;
}
u16 VECTOR_Traverse(Vector *vector, void(*callback) (MemoryNode *)){

  for(int i = 0; i < vector->tail_ ; i++){

     callback(&vector->node_[i]);

 }

return kErrorCode_Ok;
}
