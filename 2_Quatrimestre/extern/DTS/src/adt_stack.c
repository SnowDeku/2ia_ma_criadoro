// adt_stack.c : 
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//

#include "../include/adt_stack.h"
#include "memory_node.c"


Stack* STACK_Create(){

  Stack *stack;
  //Reservo el espacio en memoria para el v
  stack = (Stack *) malloc(sizeof(Stack));
  stack->head_ = 0;
  stack->tail_ = 0;
  stack->capacity_ = 0;
  stack->node_  = NULL;

  return stack;
}

s16 STACK_Init(Stack *stack ,u16 capacity){

  //Size of the stack
  stack->capacity_ = capacity;
  //Save memory * the capacity to create a stack of mem_nodes
  stack->node_  = malloc(sizeof(MemoryNode)*capacity);

  MemoryNode *aux ;
  aux = stack->node_;
  for(u8 i = 0; i < capacity ; i++){
    aux[i] = *MEMNODE_Create();
    
  }

  return kErrorCode_Memory;
}

u16 STACK_Capacity(Stack *stack){
  return stack->capacity_;
}

u16 Destroy(Stack *stack){

    for(int i = stack->tail_ ; i > 0 ; --i){

    free(&stack->node_[i]);

 }
  free(stack);
 return kErrorCode_Ok;
}

bool isEmpty(Stack *stack){
    if(stack->tail_ == 0){
      return true;
    }else{
      return false;
    }
}

bool isFull(Stack *stack){
    if(stack->tail_ == stack->capacity_){
      return true;
    }else{
      return false;
    }
}
u16 Length(Stack *stack){
  return stack->tail_;
}

Stack *Resize(Stack *stack,u16 new_capacity_){
  printf("\n Resizing stack... %d\n",new_capacity_);
  //Create a aux stack were we going to save all the data
    u16 aux_tail = stack->tail_;
   MemoryNode *node_test = malloc(sizeof(MemoryNode)*new_capacity_);


  for(u8 i = 0; i < new_capacity_ ; i++){
    node_test[i] = *MEMNODE_Create();

  }
  for(u8 i = 0; i < aux_tail ; i++){
    node_test[i] = stack->node_[i];
  }
  stack->node_ = node_test;
  stack->capacity_ = new_capacity_;
  stack->tail_ = aux_tail;

  return stack;
}

s16 InsertFirst(Stack *stack,void *value,u16 bytes){

  Insert(stack, value,stack->head_,bytes);

  return kErrorCode_Ok;
}

s16 InsertLast(Stack *stack,void *value,u16 bytes){


   Insert(stack, value, stack->tail_,bytes);

  return kErrorCode_Ok;

}

s16 Insert(Stack *stack, void *value, u16 position_,u16 bytes){

  MemoryNode *aux;
  aux = stack->node_;
   u8 change_tail_ = stack->tail_;
  //If just want to add a new value on the array 1 means Next to increase tail_

if(stack->tail_ <= stack->capacity_){

   //Change a especific position
       if( position_ <= stack->capacity_){
          for(int i = stack->tail_; i  >= position_ ; --i){
                    stack->node_[i+1] = stack->node_[i];
               }
                stack->node_[position_].ops_->setData(&stack->node_[position_],value,bytes);
               stack->tail_++;           
       }
     
  }else{
          printf(" \nThe stack is full Resize for insert more data\n");

  }

  return kErrorCode_Ok;
}

 s16 Print(Stack *stack){
    MemoryNode *node_test ;

    node_test = stack->node_;

     for(int i = 0; i< stack->tail_ ; i++){
        node_test->ops_->Print(&node_test[i]);
    }

    node_test = NULL;
return kErrorCode_Ok;
}

 MemoryNode **Head(Stack *stack){// Head: returns a reference to the first node
  return &(&stack->node_[0]); 
}

 MemoryNode *ExtractFirst(Stack *stack){
  return Extract(stack,stack->head_);
}

 MemoryNode *ExtractLast(Stack *stack){
  return Extract(stack,stack->tail_-1);
}

 MemoryNode *Extract(Stack *stack, u16 position_){
  
 if(position_ <= stack->capacity_){
      MemoryNode *aux = MEMNODE_Create() ;
       *aux = stack->node_[position_];
        //Extract a especific node
       for(int i = position_; i  < stack->tail_ ; ++i){
                  (stack->node_[i]) =
                   (stack->node_[i+1]);         
             }
               stack->tail_--;

    return aux;
 }
  

  MemoryNode *kError_Node = NULL;
  printf("\n Error trying to Extract a NULL NODE\n");
  return kError_Node;
}
u16 Concat(Stack* src,Stack* dest){
  MemoryNode *aux;
   Resize(dest,(dest->tail_+src->tail_)-2);
       for(int i = 0; i< src->tail_ ; i++){
            
            aux = Extract(src,i);
            InsertLast(dest,aux->data_,aux->size_);
  }
            free(aux);
            aux = NULL;
  
return kErrorCode_Ok;
}
u16 STACK_Traverse(Stack *stack, void(*callback) (MemoryNode *)){

  for(int i = 0; i < stack->tail_ ; i++){

     callback(&stack->node_[i]);

 }

return kErrorCode_Ok;
}
