// platform_types.h
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//
#ifndef __PLATFORM_TYPES_H__
#define __PLATFORM_TYPES_H__

#include <stdint.h>

typedef uint8_t 		u8;   //unsigned char 0<->254
typedef int_least8_t 	s8;   //char -127<->127
typedef uint16_t 		u16;  //unsigned short int 
typedef int_least16_t 	s16;  //shor int
typedef uint32_t 		u32;  //unsigned int
typedef int_least32_t 	s32;  //int
typedef uint64_t 		u64;  //unsigned long (long)
typedef int_least64_t	s64;  //float

typedef uint8_t 		bool;
#define true 			1
#define false 			0

typedef unsigned int BIT_FIELD;

#endif
