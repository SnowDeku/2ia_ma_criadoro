// common_def.h
// Toni Barella
// Algoritmos & Inteligencia Artificial
// ESAT 2016/2017
//
#ifndef __COMMON_DEF_H__
#define __COMMON_DEF_H__

#define VERBOSE_

typedef enum
{
	kErrorCode_Ok = 0,
	kErrorCode_Memory = -1, // In case malloc fails
	kErrorCode_File = -2, // In case there is no file
	kErrorCode_NULL = -3,  // In case some pointer is poiting NULL
	kErrorCode_Node_NULL = -4  // In case the node is NULL

} ErrorCode;

#endif // __COMMON_DEF_H__